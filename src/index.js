import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
//import './css/bootstrap.min.css'


class Contenedor extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      filterText: "",
    }
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
  }

  handleFilterTextChange(filterText){
    console.log("cambios en el filtro = ",filterText);
    this.setState({
      filterText:filterText
    });
  }

  render(){
    return(
      <div>
          <Buscador onFilterTextChange={this.handleFilterTextChange}/>
          < TablaDePruebas fens={this.props.fens}
                           filterText={this.state.filterText}/>
       </div>
    );
  }
}

class Buscador extends React.Component{
  constructor(props){
    super(props);
      this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
  }
  handleFilterTextChange(e){
    this.props.onFilterTextChange(e.target.value)
  }
  render(){
    return(
      <input
      type="text"
      placeholder="filtrar..."
      onChange={this.handleFilterTextChange}
      value={this.props.filterText}
      />
    );
  }
}

class TablaDePruebas extends React.Component{
  render(){
    const rows = [];
    const filterText = this.props.filterText;
    this.props.fens.forEach((fen)=>{
      if (fen.id_fen.indexOf(filterText) === -1) {
        return;
      }
      rows.push(
        <TableBody
          fen={fen}
          key={fen.id_fen}
            />
      );
    });

    return(
      <table className="table table-striped">
        <thead>
          <tr>
            <th>code</th>
            <th>id_fen</th>
            <th>comuna</th>
            <th>nodos</th>
            <th>cuadrantes</th>
            <th>duracion</th>
            <th>FechaEvento</th>
            <th>servicios</th>
            <th>FechaSolucion</th>
            <th>CantidadClientes</th>
            <th>EstadoFen</th>
            <th>AplicaDescto</th>
            <th>acciones</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table>
    );
  }
}

class TableBody extends React.Component{
  render(){
    const lineaFen = this.props.fen;
    return(
            <tr>
               <td>{lineaFen.code}</td>
               <td>{lineaFen.id_fen}</td>
               <td>{lineaFen.comuna}</td>
               <td>{lineaFen.nodos}</td>
               <td>{lineaFen.cuadrantes}</td>
               <td>{lineaFen.duracion}</td>
               <td>{lineaFen.FechaEvento}</td>
               <td>{lineaFen.servicio}</td>
               <td>{lineaFen.FechaSolucion}</td>
               <td>{lineaFen.CantidadClientes}</td>
               <td>{lineaFen.EstadoFen}</td>
               <td>{lineaFen.AplicaDescto}</td>
               <td>{lineaFen.acciones}</td>
             </tr>
    );
  }
}

const FENS = [
  {code: '1', id_fen: '1', comuna: 'Doe', nodos: 'john@gmail.com', cuadrantes: 'pp',
   duracion: 'ii', FechaEvento: 'oo', servicio: 'pp', FechaSolucion: 'pp',
   CantidadClientes: 'oo', EstadoFen: 'pp', AplicaDescto: 'pp', acciones: 'pp'},
   {code: '1', id_fen: '2', comuna: 'Doe', nodos: 'john@gmail.com', cuadrantes: 'pp',
 duracion: 'ii', FechaEvento: 'oo', servicio: 'pp', FechaSolucion: 'pp',
 CantidadClientes: 'oo', EstadoFen: 'pp', AplicaDescto: 'pp', acciones: 'pp'},
 {code: '1', id_fen: '3', comuna: 'Doe', nodos: 'john@gmail.com', cuadrantes: 'pp',
 duracion: 'ii', FechaEvento: 'oo', servicio: 'pp', FechaSolucion: 'pp',
 CantidadClientes: 'oo', EstadoFen: 'pp', AplicaDescto: 'pp', acciones: 'pp'}
];

ReactDOM.render(
  <Contenedor fens={FENS}/>,
  document.getElementById('container')
);
